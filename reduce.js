
function reduce(elements, cb, startingValue)
{
    if (typeof elements === 'undefined')
    {
        console.log([]);
        return;
    }
    if (typeof cb === 'undefined')
    {
        console.log([]);
        return;
    }
    else if(elements.length <= 0) //Added a checking for an empty inventory.
    {
        console.log([]);
        return;
    }
    else if (typeof cb !== 'function')
    {
        console.log([]);
        return;
    }
    else if (Array.isArray(elements) === false)
    {
        console.log([]);
        return;
    }
    else if(Array.isArray(elements) === true && typeof cb === 'function')
    {
        if(typeof startingValue === 'undefined')
        {
            startingValue = elements[0];
            let array = 0;
            for(let i = 1; i<elements.length; i++)
            {
                array = cb(startingValue,elements[i],i,elements);
               
                startingValue = array;
                // console.log(ans+" "+startingValue+" "+elements[i]);
                // console.log(startingValue,elements[i]);
                // ans = ans + cb(startingValue,elements[i]);
                // startingValue = ans;
            }
             return array;
        }
        else if(typeof startingValue === 'number')
        {
            let array;
            startingValue = startingValue + elements[0];
            for(let i = 1; i<elements.length; i++)
            {
                // console.log(ans+" "+startingValue+" "+elements[i]);
                // console.log(startingValue,elements[i]);
                array = cb(startingValue,elements[i]);
                // console.log(ans);
                startingValue = array;
            }
            return array;
        }
        else
            console.log([]);
    }
    else
    {
        console.log([]);
        return;
    }
}
module.exports = reduce;