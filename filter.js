
function filter(elements, cb)
{
    let array = [], position = 0;
    if (typeof elements === 'undefined')
    {
        console.log([]);
        return;
    }
    if (typeof cb === 'undefined')
    {
        console.log([]);
        return;
    }
    else if(elements.length <= 0) //Added a checking for an empty inventory.
    {
        console.log([]);
        return;
    }
    else if (typeof cb !== 'function')
    {
        console.log([]);
        return;
    }
    else if (Array.isArray(elements) === false)
    {
        console.log([]);
        return;
    }
    else if(Array.isArray(elements) === true && typeof cb === 'function')
    {
        for(let i = 0; i<elements.length; i++)
        {
            let result = cb(elements[i],i,elements);
            if(result === true)
            {
                array[position] = elements[i];
                position++;
            }
        }
        console.log(array);
    }
    else
    {
        console.log([]);
        return;
    }
}
module.exports = filter;