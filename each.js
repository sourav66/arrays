
function each(elements, cb)
{
    if (typeof elements === 'undefined')
    {
        // console.log([]);
        return [];
    }
    if (typeof cb === 'undefined')
    {
        // console.log([]);
        return [];
    }
    else if(elements.length <= 0) //Added a checking for an empty inventory.
    {
        // console.log([]);
        return [];
    }
    else if (typeof cb !== 'function')//Added a checking for cb not being a function
    {
        // console.log([]);
        return [];
    }
    else if (Array.isArray(elements) === false)
    {
        // console.log([]);
        return [];
    }
    else if(Array.isArray(elements) === true && typeof cb === 'function')
    {
        for(let i = 0; i<elements.length; i++)
        {
            cb(elements[i],i);
        }
    }
    else
    {
        // console.log([]);
        return [];
    }
}
module.exports = each;