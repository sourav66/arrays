
const elements = [1,2,3,4,5] ;

function display()
{
    const each = require("../each");
    const result = each(elements,cb);
    if(Array.isArray(result) === true)
        console.log(result);

}
function cb(item, index)
{
    console.log(index+" "+item);
}

display();
